resource "aws_efs_file_system" "main" {
  creation_token = var.efs_creation_token
  encrypted      = true
}

resource "aws_security_group" "mount_target" {
  name   = var.mount_target_sg_name
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = var.efs_sg_ingress_port
    to_port     = var.efs_sg_ingress_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.open_ipv4_cidr]
    ipv6_cidr_blocks = [var.open_ipv6_cidr]
  }

}

resource "aws_efs_mount_target" "main" {
  file_system_id  = aws_efs_file_system.main.id
  subnet_id       = aws_subnet.private_subnet_1.id
  security_groups = [aws_security_group.mount_target.id]
}

resource "aws_efs_mount_target" "subnet_2" {
  file_system_id  = aws_efs_file_system.main.id
  subnet_id       = aws_subnet.private_subnet_2.id
  security_groups = [aws_security_group.mount_target.id]
}

resource "aws_efs_access_point" "main" {
  file_system_id = aws_efs_file_system.main.id
  posix_user {
    gid = "1000"
    uid = "1000"
  }
  root_directory {
    creation_info {
      owner_gid   = "1000"
      owner_uid   = "1000"
      permissions = "0777"
    }
    path = var.efs_ap_root_path
  }
}
