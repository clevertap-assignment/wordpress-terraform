
resource "aws_security_group" "alb_sg" {
  name   = var.alb_sg_name
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = var.alb_ingress_port1
    to_port     = var.alb_ingress_port1
    protocol    = "tcp"
    cidr_blocks = [var.open_ipv4_cidr]
  }

  ingress {
    from_port   = var.alb_ingress_port2
    to_port     = var.alb_ingress_port2
    protocol    = "tcp"
    cidr_blocks = [var.open_ipv4_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.open_ipv4_cidr]
    ipv6_cidr_blocks = [var.open_ipv6_cidr]
  }

}

resource "aws_lb_target_group" "ip-tg" {
  name        = var.alb_tg_name
  port        = var.alb_ingress_port1
  protocol    = var.alb_tg_protocol
  target_type = var.alb_tg_target_type
  health_check {
    port = var.alb_ingress_port1
  }
  vpc_id = aws_vpc.main.id
}

resource "aws_lb" "main" {
  name               = var.alb_name
  load_balancer_type = var.alb_type
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]

}

resource "aws_lb_listener" "main" {
  load_balancer_arn = aws_lb.main.arn
  protocol          = var.alb_tg_protocol
  port              = var.alb_ingress_port1

  default_action {
    type             = var.alb_action
    target_group_arn = aws_lb_target_group.ip-tg.arn
  }
}
