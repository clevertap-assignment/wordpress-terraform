output "vpc_id" {
  value = aws_vpc.main.id
}

output "public_subnet1" {
  value = aws_subnet.public_subnet_1.id
}

output "public_subnet2" {
  value = aws_subnet.public_subnet_2.id
}

output "private_subnet1" {
  value = aws_subnet.private_subnet_1.id
}

output "private_subnet2" {
  value = aws_subnet.private_subnet_2.id
}

output "efs_fs_id" {
  value = aws_efs_file_system.main.id
}

output "rds_endpoint" {
  value = aws_db_instance.main.address
}

output "efs_access_endpoint" {
  value = aws_efs_access_point.main.id
}

output "alb_sg_id" {
  value = aws_security_group.alb_sg.id
}

output "wp_tg_id" {
  value = aws_lb_target_group.ip-tg.id
}

output "wp_alb" {
  value = "${aws_lb.main.dns_name}/wp-admin"
}