# Network.tf

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "public_subnet1_cidr" {
  type    = string
  default = "10.0.0.0/24"
}

variable "public_subnet1_az" {
  type    = string
  default = "us-east-1a"
}

variable "public_subnet2_cidr" {
  type    = string
  default = "10.0.1.0/24"
}

variable "public_subnet2_az" {
  type    = string
  default = "us-east-1b"
}

variable "private_subnet1_cidr" {
  type    = string
  default = "10.0.2.0/24"
}

variable "private_subnet1_az" {
  type    = string
  default = "us-east-1a"
}

variable "private_subnet2_cidr" {
  type    = string
  default = "10.0.3.0/24"
}

variable "private_subnet2_az" {
  type    = string
  default = "us-east-1b"
}

variable "open_ipv4_cidr" {
  type    = string
  default = "0.0.0.0/0"
}

## efs.tf

variable "efs_creation_token" {
  type    = string
  default = "assignment"
}

variable "mount_target_sg_name" {
  type    = string
  default = "wordpress-demo-mount-target"
}

variable "open_ipv6_cidr" {
  type    = string
  default = "::/0"
}

variable "efs_ap_root_path" {
  type    = string
  default = "/var/www/html"
}

variable "efs_sg_ingress_port" {
  type    = number
  default = 2049
}

## db.tf

variable "db_sg_name" {
  type    = string
  default = "RDS-SG"
}

variable "db_sg_ingress_port" {
  type    = number
  default = 3306
}

variable "db_subnet_group_name" {
  type    = string
  default = "wp-db-subnet-group"
}

variable "db_identifier" {
  type    = string
  default = "wp-db1"
}

variable "db_storage" {
  type    = number
  default = 20
}

variable "db_name" {
  type    = string
  default = "wordpress"
}

variable "db_engine" {
  type    = string
  default = "mysql"
}

variable "db_engine_version" {
  type    = string
  default = "5.7"
}

variable "db_instance_class" {
  type    = string
  default = "db.t3.micro"
}

variable "db_username" {
  type    = string
  default = "admin"
}

variable "db_parameter_grp_name" {
  type    = string
  default = "default.mysql5.7"
}

variable "db_publicly_accessible" {
  type    = bool
  default = false
}

variable "db_skip_snapshot" {
  type    = bool
  default = true
}

### alb.tf

variable "alb_sg_name" {
  type    = string
  default = "ALB-SG"
}

variable "alb_ingress_port1" {
  type    = number
  default = 80
}

variable "alb_ingress_port2" {
  type    = number
  default = 443
}

variable "alb_tg_name" {
  type    = string
  default = "wp-lb-tg"
}

variable "alb_tg_protocol" {
  type    = string
  default = "HTTP"
}

variable "alb_tg_target_type" {
  type    = string
  default = "ip"
}

variable "alb_name" {
  type    = string
  default = "wp-lb"
}

variable "alb_type" {
  type    = string
  default = "application"
}

variable "alb_action" {
  type    = string
  default = "forward"
}

### ECS.tf

variable "ecs_cluster_name" {
  type    = string
  default = "wp-fargate-cluster"
}

variable "ecs_execution_role_name" {
  type    = string
  default = "ecs-execution-role"
}

variable "ecs_task_networkmode" {
  type    = string
  default = "awsvpc"
}

variable "ecs_task_compatibilitiies" {
  type    = list(string)
  default = ["FARGATE"]
}

variable "ecs_task_cpu" {
  type    = number
  default = 1024
}

variable "ecs_task_memory" {
  type    = number
  default = 3072
}

variable "ecs_task_volume" {
  type    = string
  default = "wordpress"
}

variable "ecs_task_family" {
  type    = string
  default = "wof-tutorial"
}

variable "ecs_sg_name" {
  type    = string
  default = "wp-fargate"
}

variable "ecs_service_name" {
  type    = string
  default = "wp-service"
}

variable "ecs_autoscaling_max" {
  type    = number
  default = 10
}

variable "ecs_autoscaling_min" {
  type    = number
  default = 2
}

variable "ecs_autoscaling_policy_name" {
  type    = string
  default = "wp-autoscaling"
}