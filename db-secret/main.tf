resource "aws_secretsmanager_secret" "db-password" {
   name = "wp-db-password"
}
 
resource "aws_secretsmanager_secret_version" "db-password-version" {
  secret_id = aws_secretsmanager_secret.db-password.id
  secret_string = <<EOF
   {
    "password": "${var.db_password}"
   }
EOF
}