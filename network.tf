resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.public_subnet1_cidr

  availability_zone       = var.public_subnet1_az
  map_public_ip_on_launch = true
}


resource "aws_subnet" "public_subnet_2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.public_subnet2_cidr

  availability_zone       = var.public_subnet2_az
  map_public_ip_on_launch = true
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.private_subnet1_cidr

  availability_zone = var.private_subnet1_az
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.private_subnet2_cidr

  availability_zone = var.private_subnet2_az
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = var.open_ipv4_cidr
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table_association" "public_rt_subnet_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_rt_subnet_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_network_acl" "main" {
  vpc_id = aws_vpc.main.id

  egress {
    protocol   = "-1"
    rule_no    = 400
    action     = "allow"
    cidr_block = var.open_ipv4_cidr
    from_port  = 0
    to_port    = 0
  }

  #   egress {
  #     protocol   = "tcp"
  #     rule_no    = 300
  #     action     = "allow"
  #     cidr_block = "0.0.0.0/0"
  #     from_port  = 443
  #     to_port    = 443
  #   }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.open_ipv4_cidr
    from_port  = 0
    to_port    = 0
  }

  #   ingress {
  #     protocol   = "tcp"
  #     rule_no    = 200
  #     action     = "allow"
  #     cidr_block = "0.0.0.0/0"
  #     from_port  = 443
  #     to_port    = 443
  #   }


}

resource "aws_network_acl_association" "public_nacl_subnet_1" {
  network_acl_id = aws_network_acl.main.id
  subnet_id      = aws_subnet.public_subnet_1.id
}

resource "aws_network_acl_association" "public_nacl_subnet_2" {
  network_acl_id = aws_network_acl.main.id
  subnet_id      = aws_subnet.public_subnet_2.id
}

resource "aws_eip" "eip1" {
  vpc = true
}

resource "aws_eip" "eip2" {
  vpc = true
}

resource "aws_nat_gateway" "nat1" {
  allocation_id = aws_eip.eip1.id
  subnet_id     = aws_subnet.public_subnet_1.id


  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_nat_gateway" "nat2" {
  allocation_id = aws_eip.eip2.id
  subnet_id     = aws_subnet.public_subnet_2.id


  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_route_table" "private_route_table1" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = var.open_ipv4_cidr
    nat_gateway_id = aws_nat_gateway.nat1.id
  }
}


resource "aws_route_table" "private_route_table2" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = var.open_ipv4_cidr
    nat_gateway_id = aws_nat_gateway.nat2.id
  }
}

resource "aws_route_table_association" "private_rt_subnet_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_route_table1.id
}

resource "aws_route_table_association" "private_rt_subnet_2" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_route_table2.id
}

