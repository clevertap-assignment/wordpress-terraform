data "aws_secretsmanager_secret" "db_password" {
  arn = "arn:aws:secretsmanager:us-east-1:673025905097:secret:wp-db-password-pHUn5p"
}

data "aws_secretsmanager_secret_version" "db_password" {
  secret_id = data.aws_secretsmanager_secret.db_password.arn
}


locals {
  db_creds = jsondecode(data.aws_secretsmanager_secret_version.db_password.secret_string)
}