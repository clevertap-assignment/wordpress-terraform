resource "aws_security_group" "db_sg" {
  name   = var.db_sg_name
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = var.db_sg_ingress_port
    to_port     = var.db_sg_ingress_port
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.open_ipv4_cidr]
    ipv6_cidr_blocks = [var.open_ipv6_cidr]
  }

}

resource "aws_db_subnet_group" "wp_subnet_group" {
  name       = var.db_subnet_group_name
  subnet_ids = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
}

resource "aws_db_instance" "main" {
  identifier             = var.db_identifier
  allocated_storage      = var.db_storage
  db_name                = var.db_name
  engine                 = var.db_engine
  engine_version         = var.db_engine_version
  instance_class         = var.db_instance_class
  username               = var.db_username
  password               = local.db_creds.password
  parameter_group_name   = var.db_parameter_grp_name
  db_subnet_group_name   = aws_db_subnet_group.wp_subnet_group.name
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  publicly_accessible    = var.db_publicly_accessible
  skip_final_snapshot    = var.db_skip_snapshot
}
