
resource "aws_ecs_cluster" "main" {
  name = var.ecs_cluster_name

}

# resource "aws_iam_policy" "ecs_execution_policy" {
#   name = "ecs-execution-policy"

# #   policy = <<EOF
# #     {
# #         "Version": "2012-10-17",
# #         "Statement": [
# #             {
# #             "Effect": "Allow",
# #             "Action": [
# #                 "ecr:GetAuthorizationToken",
# #                 "ecr:BatchCheckLayerAvailability",
# #                 "ecr:GetDownloadUrlForLayer",
# #                 "ecr:BatchGetImage",
# #                 "logs:CreateLogStream",
# #                 "logs:PutLogEvents"
# #             ],
# #             "Resource": "*"
# #             }
# #         ]
# #     } 
# #   EOF

# policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "ecr:*",
#         "logs:*",
#         "ec2:Describe*"
#       ],
#       "Effect": "Allow",
#       "Resource": "*"
#     }
#   ]
# }
# EOF
# }

resource "aws_iam_role" "ecs_execution_role" {
  name = var.ecs_execution_role_name

  assume_role_policy = <<EOF
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "ecs-tasks.amazonaws.com"
                ]
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
    EOF
}

resource "aws_iam_role_policy_attachment" "ecs_execution" {
  role       = aws_iam_role.ecs_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds"

}

# resource "aws_ecs_task_definition" "wp-task-definition" {
#    network_mode = "awsvpc"
#    container_definitions = <<DEFINITION
#    [
#             {

#             "portMappings": [
#                 {
#                     "containerPort": 8080,
#                     "protocol": "tcp"
#                 }
#             ],
#             "essential": true,
#             "mountPoints": [
#                 {
#                     "containerPath": "/bitnami/wordpress",
#                     "sourceVolume": "wordpress"
#                 }
#             ],
#             "name": "wordpress",
#             "image": "673025905097.dkr.ecr.us-east-1.amazonaws.com/wordpress-sample:7.0.0",
#             "environment": [
#                 {
#                     "name": "MARIADB_HOST",
#                     "value": "${aws_db_instance.main.address}"   

#                 },
#                 {   
#                     "name": "WORDPRESS_DATABASE_USER",
#                     "value": "admin"
#                 },
#                 {   
#                     "name": "WORDPRESS_DATABASE_PASSWORD",
#                     "value": "${var.wordpressdbpassword}"
#                 },
#                 {   
#                     "name": "WORDPRESS_DATABASE_NAME",
#                     "value": "wordpress"
#                 },
#                 {   
#                     "name": "PHP_MEMORY_LIMIT",
#                     "value": "512M"
#                 },
#                 {   
#                     "name": "enabled",
#                     "value": "false"
#                 }
#             ]
#         }
#    ]
#    DEFINITION

# #    task_role_arn = aws_iam_role.ecs_execution_role.arn
#    execution_role_arn = aws_iam_role.ecs_execution_role.arn
#    requires_compatibilities = ["FARGATE"]
#    cpu = "1024"
#    memory = "3072"
#    volume {
#      name = "wordpress"
#      efs_volume_configuration {
#        file_system_id = "${aws_efs_file_system.main.id}"
#        transit_encryption = "ENABLED"
#        authorization_config {
#          access_point_id = "${aws_efs_access_point.main.id}"
#          iam = "DISABLED"
#        }
#      }
#    }

#    family = "wof-tutorial"

#    depends_on = [
#      aws_db_instance.main
#    ]

# }

resource "aws_ecs_task_definition" "wp-task-definition" {
  network_mode          = var.ecs_task_networkmode
  container_definitions = <<DEFINITION
   [
            {
            
            "portMappings": [
                {
                    "containerPort": 80,
                    "protocol": "tcp"
                }
            ],
            "essential": true,
            "mountPoints": [
                {
                    "containerPath": "${var.efs_ap_root_path}",
                    "sourceVolume": "${var.ecs_task_volume}"
                }
            ],
            "name": "${var.ecs_task_volume}",
            "image": "673025905097.dkr.ecr.us-east-1.amazonaws.com/wordpress-sample:latest",
            "environment": [
                {
                    "name": "WORDPRESS_DB_HOST",
                    "value": "${aws_db_instance.main.address}"   
                    
                },
                {   
                    "name": "WORDPRESS_DB_USER",
                    "value": "${var.db_username}"
                },
                {   
                    "name": "WORDPRESS_DB_PASSWORD",
                    "value": "${local.db_creds.password}"
                },
                {   
                    "name": "WORDPRESS_DB_NAME",
                    "value": "${var.db_name}"
                },
                {   
                    "name": "WORDPRESS_TABLE_PREFIX",
                    "value": "WP_"
                }
            ]
        }
   ]
   DEFINITION

  #    task_role_arn = aws_iam_role.ecs_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn
  requires_compatibilities = var.ecs_task_compatibilitiies
  cpu                      = var.ecs_task_cpu
  memory                   = var.ecs_task_memory
  volume {
    name = var.ecs_task_volume
    efs_volume_configuration {
      file_system_id     = aws_efs_file_system.main.id
      transit_encryption = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.main.id
        iam             = "DISABLED"
      }
    }
  }

  family = var.ecs_task_family

  depends_on = [
    aws_db_instance.main
  ]

}


resource "aws_security_group" "wp_fargate" {
  name   = var.ecs_sg_name
  vpc_id = aws_vpc.main.id

  ingress {
    from_port       = var.alb_ingress_port1
    to_port         = var.alb_ingress_port1
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.open_ipv4_cidr]
    ipv6_cidr_blocks = [var.open_ipv6_cidr]
  }
}

resource "aws_ecs_service" "wp-service" {
  cluster         = aws_ecs_cluster.main.name
  name            = var.ecs_service_name
  task_definition = aws_ecs_task_definition.wp-task-definition.arn
  load_balancer {
    target_group_arn = aws_lb_target_group.ip-tg.arn
    container_name   = var.ecs_task_volume
    container_port   = 80
  }
  desired_count                      = var.ecs_autoscaling_min
  platform_version                   = "1.4.0"
  launch_type                        = "FARGATE"
  deployment_maximum_percent         = "200"
  deployment_minimum_healthy_percent = "0"
  network_configuration {
    subnets          = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]
    security_groups  = [aws_security_group.wp_fargate.id]
    assign_public_ip = false
  }
}

resource "aws_appautoscaling_target" "wp_ecs_target" {
  max_capacity       = var.ecs_autoscaling_max
  min_capacity       = var.ecs_autoscaling_min
  resource_id        = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.wp-service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}


resource "aws_appautoscaling_policy" "wp_autoscaling" {
  name               = var.ecs_autoscaling_policy_name
  service_namespace  = aws_appautoscaling_target.wp_ecs_target.service_namespace
  scalable_dimension = aws_appautoscaling_target.wp_ecs_target.scalable_dimension
  resource_id        = aws_appautoscaling_target.wp_ecs_target.resource_id
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = 75
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}
